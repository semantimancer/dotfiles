import XMonad

import Control.Monad (forM_, join)
import Data.Char (isSpace, toLower)
import Data.Function (on)
import Data.List (intercalate, isInfixOf, sortBy, elemIndex, partition)
import Data.Maybe (isJust)
import System.Exit

import XMonad.Actions.CopyWindow
import XMonad.Actions.CycleWS (toggleWS)
import XMonad.Actions.DynamicWorkspaces
import XMonad.Actions.PerLayoutKeys
import XMonad.Actions.Sift
import XMonad.Actions.Submap
import XMonad.Actions.TagWindows
import XMonad.Actions.TopicSpace

import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.FadeInactive
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.RefocusLast (refocusLastLogHook)
import XMonad.Hooks.ServerMode

import XMonad.Layout.Accordion
import XMonad.Layout.BinarySpacePartition
import XMonad.Layout.CenteredIfSingle
import XMonad.Layout.CircleEx hiding (Rotate)
import XMonad.Layout.Column
import XMonad.Layout.ComboP
import XMonad.Layout.Drawer
import XMonad.Layout.IfMax
import XMonad.Layout.MultiToggle
import XMonad.Layout.MultiToggle.Instances
import XMonad.Layout.NoBorders
import XMonad.Layout.PerWorkspace
import XMonad.Layout.Reflect
import XMonad.Layout.ResizableThreeColumns
import XMonad.Layout.ShowWName
import XMonad.Layout.Spacing
import XMonad.Layout.Tabbed
import XMonad.Layout.TwoPane

import XMonad.Util.NamedWindows (getName)
import XMonad.Util.Run (safeSpawn)
import XMonad.Util.NamedScratchpad
import XMonad.Util.SpawnOnce

import qualified Data.Map                       as M
import qualified XMonad.StackSet                as W

startupCommands = ["autorandr -c main"
                  ,"run_wallpaper default"
                  ,"redshift"
                  ,"xcompmgr -Ff"
                  ]

myXFont = "xft:LiberationMono:size=10"

--
--
--  TOPICS
--  Topic spaces let me execute commands upon going to a workspace. Most
--  workspaces will also have a special directory that terminals will
--  automatically cd to when called.
--
--

wsKeys = [xK_1,xK_2,xK_3,xK_4,xK_5,xK_6,xK_7,xK_8,xK_9,xK_0, xK_BackSpace]

data WSTopic = WT { topicName :: String
                  , topicPath :: FilePath
                  , topicKey :: KeySym
                  , topicAction :: X ()
                  }

myTopics :: [WSTopic]
myTopics = [WT "web"        ""                    xK_1  $ spawn "floorp"
           ,WT "vim"        ""                    xK_2  $ spawnCLI "ViM" "vim"
           ,WT "chat"       ""                    xK_3  $ spawn "Discord"
           ,WT "writing"    "/Documents"          xK_4  $ buildRofi ["write"]
           ,WT "gimp"       "/Pictures"           xK_5  $ spawn "gimp"
           ,WT "work"       ""                    xK_6  $ spawnShell
           ,WT "reference"  ""                    xK_7  $ scratchpad "ranger"
           ,WT "compile"    "/Computer"           xK_8  $ spawnShell
           ,WT "game"       "/Games"              xK_9  $ spawnShell
           ,WT "movie"      "/Videos"             xK_0  $ return ()
           ,WT "portrait"   ""                    xK_BackSpace $ return ()
           ,WT "music"      "/Music"              xK_0  $ spawnCRT "cmus"
           ,WT "obsidian"   "/Documents/Obsidian" xK_6  $ spawn "obsidian"
           ,WT "view"       "/Pictures"           xK_7  $ spawnCLI "View" "ranger"
           ,WT "pdf"        "/Documents"          xK_7  $ scratchpad "ranger"
           ,WT "steam"      "/Games"              xK_9  $ spawn "steam"
           ,WT "homunculus" "/Games/homunculus"   xK_8  $ spawn "homunculus"
           ]

myTopicConfig :: TopicConfig
myTopicConfig = def
  { topicDirs = M.fromList $ map (\(WT n p _ _) -> (n,"/home/ben"++p)) myTopics
  , defaultTopicAction = const spawnShell
  , defaultTopic = "web"
  , topicActions = M.fromList $ map (\(WT n _ _ a) -> (n,a)) myTopics
  }

buildTopicSwitches :: [WSTopic] -> [(KeySym, X ())]
buildTopicSwitches [] = []
buildTopicSwitches (t:ts) = nextItem : buildTopicSwitches otherKeys
  where (thisKey,otherKeys) = partition ((==) (topicKey t) . topicKey) ts
        nextItem = if (length thisKey)==0
                   then (topicKey t, switchTopic myTopicConfig $ topicName t)
                   else (topicKey t, cycleWorkspaces $ map topicName $ t:thisKey)

--I use a case statement to explicitly give the Orderings because I want to treat
--the Nothing case opposite of how it's treated with compare by default
sortStringsByTopic :: [String] -> [String]
sortStringsByTopic = sortBy $ \x y -> case (elemIndex x ts,elemIndex y ts) of
                                        (Nothing,Nothing) -> EQ
                                        (Nothing,_)       -> GT
                                        (_,Nothing)       -> LT
                                        (Just x',Just y') -> compare x' y'
  where ts = map topicName myTopics

myScratchpads :: NamedScratchpads
myScratchpads = [NS "ranger" "urxvt -title ranger -e ranger" (title =? "ranger") $ centerBox 0.5
                ,NS "urxvt" "urxvt -title scratchpad" (title =? "scratchpad") $ centerBox 0.5
                ,NS "pavucontrol" "pavucontrol" (title =? "Volume Control") $ centerBox 0.5
                --,NS "cmus" "cool-retro-term -T cmus -e cmus" (icon =? "cmus") $ centerBox 0.5
                ,NS "gtop" "urxvt -title gtop -e gtop" (title =? "gtop") $ centerBox 0.8
                ]
  where centerBox n = customFloating $ myRect n
        icon = stringProperty "WM_ICON_NAME"

scratchpad :: String -> X ()
scratchpad = namedScratchpadAction myScratchpads

myRect :: Rational -> W.RationalRect
myRect n = let i = (1.0-n)/2 in W.RationalRect i i n n

spawnShell :: X ()
spawnShell = currentTopicDir myTopicConfig >>= spawnShellIn

spawnShellIn :: Dir -> X ()
spawnShellIn dir = spawn $ "urxvt -cd \""++dir++"\""

spawnCLI :: String -> String -> X ()
spawnCLI x y = spawn $ ". /home/ben/.bashrc; urxvt -title "++x++" -e "++y

spawnCRT :: String -> X ()
spawnCRT = spawn . (++) "cool-retro-term -e "

goto :: WorkspaceId -> X ()
goto = switchTopic myTopicConfig

createAndCopy :: WorkspaceId -> X ()
createAndCopy w = newWorkspace w >> windows (copy w)

createAndShift :: WorkspaceId -> X ()
createAndShift w = newWorkspace w >> windows (W.shift w)

createOrGoto :: WorkspaceId -> X ()
createOrGoto w = do
  exists <- workspaceExist w
  if exists then goto w else newWorkspace w >> switchTopic myTopicConfig w

newWorkspace :: WorkspaceId -> X ()
newWorkspace w = do
  exists <- workspaceExist w
  if exists then return () else addHiddenWorkspace w

workspaceExist :: WorkspaceId -> X Bool
workspaceExist w = do
  xs <- get
  return $ workspaceExists w (windowset xs)

workspaceExists :: WorkspaceId -> W.StackSet WorkspaceId l a s sd -> Bool
workspaceExists w ws = w `elem` map W.tag (W.workspaces ws)

getPossibleWorkspaces :: X [WorkspaceId]
getPossibleWorkspaces = do
  ws <- get
  return $ ts++(filter (`notElem` ts) (existingWorkspaces (windowset ws)))
  where ts = map topicName myTopics

existingWorkspaces :: W.StackSet WorkspaceId l a s sd -> [String]
existingWorkspaces ws = map W.tag $ W.workspaces ws

usedWorkspaces :: W.StackSet i l a s sd -> [W.Workspace i l a]
usedWorkspaces = (filter (isJust . W.stack)) . W.workspaces

cycleWorkspaces :: [WorkspaceId] -> X ()
cycleWorkspaces ws = do
  winset <- gets windowset

  let cur = W.currentTag winset
      used = sortStringsByTopic $ map W.tag $ usedWorkspaces winset
      ws' = (\(x,y) -> x++y) $ partition (`elem` used) ws

  createOrGoto $ case elemIndex cur ws' of
    Nothing -> head ws'
    Just i  -> ws'!!((i+1) `mod` (length ws'))

--
--
--  PROMPTS
--  A sizable part of this setup is going to be devoted to prompts, which
--  were originally all from Xmonad.Prompt, but which are now being phased
--  into custom rofi scripts.
--
--

rofiOpen :: X ()
rofiOpen = spawn "rofi -modi combi -show combi -combi-modi window,run"

buildRofi :: [String] -> X ()
buildRofi strs = spawn $ concat ["rofi -modi \""
                                ,intercalate "," $ map f strs
                                ,"\" -show "
                                ,head strs]
  where f "qalc"    = "qalc:rofi-qalc"
        f "kill"    = "kill:rofi-kill"
        f "info"    = "info:rofi-info"
        f "write"   = "write:rofi-write"
        f "search"  = "search:rofi-search"
        f "goto"    = "goto:rofi-goto"
        f "shift"   = "shift:rofi-shift"
        f "copy"    = "copy:rofi-copy"
        f _ = "" --This will cause an error that prevents rofi from starting

--
--
--  KEYBINDINGS
--  The basic idea here is a modal shortcut system. There are four modes that 
--  XMonad can be in: default mode, visual mode, command mode, and audio mode. 
--  The modes are made with submaps of keybindings, most of which call the 
--  submap again.
--
--
--
--

keyboard conf@(XConfig {XMonad.modMask = modm}) = M.fromList $
  --  Default mode has a few convenience functions (ones that I use too often to 
  --  always have to switch modes) and then shortcuts for going into command,
  --  visual, and audio modes.
  [ ((modm              , xK_Return), spawnShell)
  , ((modm .|. shiftMask, xK_Return), currentTopicAction myTopicConfig)

  , ((modm              , xK_v), submap . M.fromList $ visualMode)
  , ((modm              , xK_semicolon), submap . M.fromList $ commandMode)
  , ((modm              , xK_m), submap . M.fromList $ audioMode)

  --  Everything below this point (including the three mapped lists) is here 
  --  because I use these commands too much for me to have to enter a specific 
  --  mode every time I use one
  , ((modm              , xK_j), windows W.focusDown)
  , ((modm              , xK_k), windows W.focusUp)
  , ((modm .|. shiftMask, xK_s), spawn "autorandr --change")
  , ((modm              , xK_equal), spawn "dunstctl history-pop")
  , ((modm .|. shiftMask, xK_equal), spawn "dunstctl close-all")

  --And a safety function
  , ((modm .|. shiftMask, xK_q), io (exitWith ExitSuccess))
  ] ++ navKeys
  where 
    --  Command mode is a submap for spawning new processes, calling in XMonad 
    --  prompts, and generally executing actions I find myself doing a lot.
    commandMode =
      --Command mode is mainly going to be used for prompts...
      [ ((0, xK_o), rofiOpen)
      , ((0, xK_c), buildRofi ["qalc"])
      , ((0, xK_i), buildRofi ["info"])
      , ((0, xK_k), buildRofi ["kill"])
      , ((0, xK_s), buildRofi ["search"])
  
      -- ...but occasionally I'll use it to spawn something else
      , ((0, xK_Return), spawn $ XMonad.terminal conf)
      , ((0, xK_t), scratchpad "urxvt")
      , ((0, xK_r), scratchpad "ranger")
      , ((0, xK_p), scratchpad "pavucontrol")
      , ((0, xK_u), spawnCRT "cmus")
      , ((0, xK_g), scratchpad "gtop")

      , ((0, xK_m), submap . M.fromList $ audioMode)
      , ((0, xK_v), submap . M.fromList $ visualMode)
      ] ++ navKeys
    --  Visual mode is for manipulating the windows on the screen. This means 
    --  changing focus, rearranging tiles, etc. are handled almost exclusively 
    --  in visual mode
    visualMode =
      -- Basic navigation 
      [ ((0, xK_j), windows W.focusDown >> visual)
      , ((0, xK_k), windows W.focusUp >> visual)

      -- Backgrounds
      , ((0, xK_w), spawn "run_wallpaper default")
      , ((shiftMask, xK_w), spawn "run_wallpaper")
      , ((0, xK_x), spawn "xtoggle")
      , ((shiftMask, xK_x), withFocused $ toggleTag "no-fade")

      -- Moving tiles around
      , ((shiftMask, xK_j), windows siftDown >> visual)
      , ((shiftMask, xK_k), windows siftUp >> visual)
      , ((0, xK_y), sendMessage SelectNode >> visual)
      , ((0, xK_p), sendMessage MoveNode)
      , ((0, xK_Return), sendMessage Swap)
      , ((shiftMask, xK_Return), sendMessage Rotate)

      , ((0, xK_d), kill1)
      , ((0, xK_space), sendMessage NextLayout >> visual)
      , ((0, xK_s), (withFocused $ windows . W.sink) >> visual)
      , ((0, xK_f), (withFocused $ \w -> windows $ \s -> W.float w (myRect 0.5) s))
      , ((0, xK_z), (sendMessage $ Toggle FULL) >> visual)

      --  control+{h,j,k,l controls sizing of tiles
      , ((controlMask, xK_l), sendMessage (MoveSplit R) >> sendMessage Expand >> visual)
      , ((controlMask, xK_h), sendMessage (MoveSplit L) >> sendMessage Shrink >> visual)
      , ((controlMask, xK_j), sendMessage (MoveSplit D) >> sendMessage MirrorShrink >> visual)
      , ((controlMask, xK_k), sendMessage (MoveSplit U) >> sendMessage MirrorExpand >> visual)

      --  control+shift+{j,k} modifies number of master windows
      , ((controlMask .|. shiftMask, xK_j), sendMessage (IncMasterN (-1)) >> visual)
      , ((controlMask .|. shiftMask, xK_k), sendMessage (IncMasterN 1) >> visual)

      --  Visual mode is also where you go to switch topics with o or O, or copy
      --  windows to other workspaces with c (ALL workspaces with C). 
      --  o goes to a topic, shift+O drags the focused window with you.
      , ((0, xK_o), buildRofi ["goto"])
      , ((shiftMask, xK_o), buildRofi ["shift"])
      , ((0, xK_c), buildRofi ["copy"])
      , ((shiftMask, xK_c), windows copyToAll)

      -- Switching to other modes
      , ((0, xK_semicolon), submap . M.fromList $ commandMode)
      , ((0, xK_m), submap . M.fromList $ audioMode)
      ] ++ navKeys
    visual = submap . M.fromList $ visualMode
    --Audio Mode deals with volume
    audioMode = 
      --Volume controls first
      [ ((0, xK_m), spawn "pamixer -t" >> audio)

      , ((0, xK_j),           spawn "pamixer --decrease 10")
      , ((shiftMask, xK_j),   spawn "pamixer --decrease 20")
      , ((controlMask, xK_j), spawn "pamixer --decrease 5")
      , ((0, xK_k),           spawn "pamixer --increase 10")
      , ((shiftMask, xK_k),   spawn "pamixer --increase 20")
      , ((controlMask, xK_k), spawn "pamixer --increase 5")

      , ((0, xK_1), spawn "pamixer --set-volume 10" >> audio)
      , ((0, xK_2), spawn "pamixer --set-volume 20" >> audio)
      , ((0, xK_3), spawn "pamixer --set-volume 30" >> audio)
      , ((0, xK_4), spawn "pamixer --set-volume 40" >> audio)
      , ((0, xK_5), spawn "pamixer --set-volume 50" >> audio)
      , ((0, xK_6), spawn "pamixer --set-volume 60" >> audio)
      , ((0, xK_7), spawn "pamixer --set-volume 70" >> audio)
      , ((0, xK_8), spawn "pamixer --set-volume 80" >> audio)
      , ((0, xK_9), spawn "pamixer --set-volume 90" >> audio)
      , ((0, xK_0), spawn "pamixer --set-volume 100" >> audio)

      , ((0, xK_Return), spawnCRT "cmus")
      , ((0, xK_space), spawn "cmus-remote -u")
      , ((0, xK_h), spawn "cmus-remote -r")
      , ((0, xK_l), spawn "cmus-remote -n")

      , ((0, xK_semicolon), submap . M.fromList $ commandMode)
      , ((0, xK_v), submap . M.fromList $ visualMode)
      ] ++ navKeys
    audio = submap . M.fromList $ audioMode
    --Navigation keys take care of movement between screens and topic spaces
    navKeys =
      [((modm .|. m, key), (screenWorkspace sc >>= flip whenJust (windows . f)))
        | (key, sc) <- zip [xK_w, xK_e, xK_r] [0..]
        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]
      ++
      --buildTopicSwitches sets each key up with either switchTopic or cycleWorkspaces
      [((modm, k), f) | (k,f) <- buildTopicSwitches myTopics]
      ++
      --mod+shift+number shifts to primary workspace for that number.
      --No cycleWorkspaces logic here because I don't know how on earth that'd be convenient.
      [((modm .|. shiftMask, i), (windows . W.shift) j)
        | (i, j) <- zip wsKeys (map topicName myTopics)]

mouse (XConfig {XMonad.modMask = modm}) = M.fromList $
  [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w >> windows W.shiftMaster)) ]

--
--
--  LAYOUT
--  The basic layout is a three-column setup that's essentially CenterMainFluid but with
--  resizing of side windows, or alternatively BinarySpacePartition (if I need something
--  more specific) or a tabbed layout (if I want fullscreen).
--
--  There are also custom layouts for particular named workspaces. Each of these layouts
--  is given its own line below the "where" and then strung together at the end with the
--  perWS function.
--
--  mkToggle allows for zooming in on windows. This zoom (Z in visual mode) combined with
--  the Tabbed layout replace the default Full layout
--
--

layout = showWName' (def { swn_font = myXFont }) $ avoidStruts $ mkToggle (single FULL) $ perWS 
       $ smartCol ||| smartBSP ||| greyTabbed
  where borders = spacingRaw True (Border 3 3 3 3) True (Border 3 3 3 3) True
        center = centeredIfSingle 0.7 0.99
        smartAdj = center . borders

        smartBSP = smartAdj emptyBSP
        smartCol = smartAdj  $ ResizableThreeColMid 1 (3/100) (60/100) []
        vertical = (borders Accordion) ||| (borders $ Column 1.3)
        greyTabbed = tabbed shrinkText myTabConfig
        gimp' dir = let (r:rs) = map Role ["gimp-toolbox-1","gimp-dock-1"]
                    in dir (drawer 0.05 0.2 (foldr Or r (tail rs)) Accordion) greyTabbed

        vim =       onWorkspace "vim"       $ smartBSP ||| vertical
        chat =      onWorkspace "chat"      $ (reflectHoriz circle)
        writing =   onWorkspace "writing"   $ greyTabbed ||| circle
        gimp =      onWorkspace "gimp"      $ gimp' onRight ||| gimp' onLeft
        reference = onWorkspace "reference" $ vertical ||| smartBSP
        music =     onWorkspace "music"     $ IfMax 2 vertical (smartBSP ||| greyTabbed)
        steam =     onWorkspace "steam"     $ greyTabbed
        movie =     onWorkspace "movie"     $ (IfMax 2 vertical greyTabbed) ||| circle
        portrait =  onWorkspace "portrait"  $ vertical ||| greyTabbed

        pdf =       onWorkspace "pdf"       $ vertical ||| smartBSP
        obsidian =  onWorkspace "obsidian"  $ greyTabbed ||| vertical

        perWS = vim . chat . writing . gimp . reference . steam . music
              . obsidian . portrait . pdf . movie

--  Theme used for Tabbed layout
myTabConfig = def { inactiveBorderColor = "#a9a9a9"
                  , inactiveColor = "#a1a1a1"
                  , inactiveTextColor = "#363636"
                  , activeBorderColor = "#a9a9a9"
                  , activeColor = "#363636"
                  , activeTextColor = "#d3d3d3"
                  , fontName = "xft:LiberationMono:size=10"
                  }

--
--
--  MANAGE HOOK
--  Fairly standard manage hook. Some windows, when spawned, are moved to one 
--  of the named workspaces designed for that type of program.
--
--  The manage hook for the new scratchpad copies it to all workspaces, but because
--  it's floating, it actually shows up on the *other* screen.
--
--

myManageHook = composeAll
  [ className =? "MPlayer"        --> doShift "movie"
  , className =? "mpv"            --> doShift "movie"
  , className =? "Gimp"           --> doShift "gimp"
  , className =? "discord"        --> doShift "chat"
  , className =? "Mumble"         --> doShift "chat"
  , className =? "steam"          --> doShift "steam"
  , className =? "music"          --> doShift "music"
  , className =? "movie"          --> doShift "movie" <+> (ask >>= doF . W.sink)
  , className =? "forge"          --> doShift "game"
  , title =? "ViM"                --> doShift "vim"
  , title =? "ViM-Write"          --> doShift "writing"
  , isFullscreen                  --> doFullFloat
  , isDialog                      --> doFloat
  , namedScratchpadManageHook myScratchpads
  ]

--
--
--  LOG HOOK
--  This gets all windows, figures out the current window title and the current visible/active
--  workspaces, then outputs that information to a couple pipes inside /tmp for polybar to use.
--
--

myLogHook = do
  fadeOutLogHook . fadeIf fadeHook $ 0.7

  refocusLastLogHook
  nsHideOnFocusLoss myScratchpads
  where fadeHook = isUnfocused 
              <&&> (className /? "movie") 
              <&&> (skipTag "no-fade")
              <&&> (title /? "Picture-in-Picture")

--This is XMonad.ManageHook.(=?), but with /=
(/?) :: Eq a => XMonad.Query a -> a -> XMonad.Query Bool
q /? x = fmap (/= x) q

--This is a Query conversion of XMonad.Actions.TagWindows.hasTag, but with notElem
skipTag :: String -> XMonad.Query Bool
skipTag s = ask >>= (\w -> liftX $ fmap (s `notElem`) (getTags w))

toggleTag :: String -> Window -> X ()
toggleTag tag w = do
  check <- hasTag tag w
  if check then delTag tag w else addTag tag w

visibleTag :: W.StackSet i l a s sd -> [i]
visibleTag = (map $ W.tag . W.workspace) . W.visible

currentLayout :: W.StackSet i l a s sd -> l
currentLayout = W.layout . W.workspace . W.current

--
--
--  EVENT HOOK
--  This handles incoming events. Beyond the defaults for XMonad and EWMH functionalities, this
--  listens for some custom XMONAD_ commands sent via xmonadctl and responds accordingly. In all,
--  it's way more complicated than it needs to be but this is the only way I can find to actually 
--  get this functionality. Everything else runs into problems with rofi hanging.
--
--

myEventHook = handleEventHook def 
              <+> (serverModeEventHookF "XMONAD_GOTO" createOrGoto)
              <+> (serverModeEventHookF "XMONAD_SHIFT" createAndShift)
              <+> (serverModeEventHookF "XMONAD_COPY" createAndCopy)
              <+> (serverModeEventHookF "XMONAD_COMMAND" 
                    (\x -> spawn $ "notify-send \"Unknown XMONAD_COMMAND\" \""++x++"\""))

main :: IO ()
main = xmonad $ docks $ ewmhFullscreen . ewmh $ def
    { terminal            = "urxvt"
    , focusFollowsMouse   = False
    , clickJustFocuses    = False
    , borderWidth         = 0
    , modMask             = mod1Mask
    , workspaces          = map topicName myTopics
    , keys                = keyboard
    , mouseBindings       = mouse
    , layoutHook          = layout
    , manageHook          = myManageHook
    , logHook             = myLogHook
    , startupHook         = mapM_ spawnOnce startupCommands
    , handleEventHook     = myEventHook
    }
