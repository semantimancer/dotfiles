# https://nixos.org/manual/nixpkgs/stable/#sec-overlays-definition
[
(self: super:
{
  haskellPackages = super.haskellPackages.override (old: {
    overrides = self.lib.composeExtensions (old.overrides or (_: _: {}))
      (
        hself: hsuper: {
          xmonad = hsuper.xmonad_0_18_0;
        }
      );
  });
}
)
]
