# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix

      ./apps/vim
      ./apps/ranger
      ./apps/steam.nix
      ./apps/rofi-modules.nix
      ./apps/misc-modules.nix
      ./apps/programs.nix
    ];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "Este"; # Define your hostname.
  networking.extraHosts = "10.0.0.29 elendil";

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "America/New_York";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "en_US.UTF-8";
    LC_IDENTIFICATION = "en_US.UTF-8";
    LC_MEASUREMENT = "en_US.UTF-8";
    LC_MONETARY = "en_US.UTF-8";
    LC_NAME = "en_US.UTF-8";
    LC_NUMERIC = "en_US.UTF-8";
    LC_PAPER = "en_US.UTF-8";
    LC_TELEPHONE = "en_US.UTF-8";
    LC_TIME = "en_US.UTF-8";
  };

  fonts.packages = with pkgs; [
    corefonts
    font-awesome_4
    (nerdfonts.override { fonts = [ "FiraCode" "FiraMono" "NerdFontsSymbolsOnly" ]; })
  ];

  environment.homeBinInPath = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.ben = {
    isNormalUser = true;
    description = "Ben";
    extraGroups = [ "networkmanager" "wheel" ];
    packages = with pkgs; [];
  };

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  services.displayManager = {
    defaultSession = "none+xmonad";
    autoLogin.enable = true;
    autoLogin.user = "ben";
  };

  services.xserver = {
    xkb.layout = "us";
    xkb.variant = "";

    enable = true;
    autorun = true;

    videoDrivers = [ "amdgpu" ];

    displayManager.lightdm.enable = true;
    
    windowManager.xmonad = {
      enable = true;
      enableContribAndExtras = true;
    };
  };

  # Enable the OpenSSH daemon.
  services.openssh = {
    enable = true;
  };

  services.autorandr = {
    enable = true;
    defaultTarget = "main";
  };

  services.lorri.enable = true;

  services.blueman.enable = true;

  services.syncthing = {
    enable = true;
    user = "ben";
    dataDir = "/home/ben/Documents";
    configDir = "/home/ben/.config/syncthing";
  };

  systemd.services = {
    remote-backup = {
      path = ["/run/current-system/sw"];
      environment = {
        # Not certain this is needed, but it may help with rdiff-backup finding the SSH keys if it looks in ~/.ssh
        HOME = "/home/ben";
      };
      script = "rdiff-backup backup --exclude-globbing-filelist /home/ben/.excludes /home/ben/ ben@elendil::/backupdrive/home/ben/";
      serviceConfig.User = "ben";
      startAt = "*-*-* 10:30:00";

      enable = true;
    };

    run-wallpaper = {
      path = ["/run/current-system/sw"];
      environment = {
        HOME = "/home/ben";
      };
      script = "run_wallpaper";
      serviceConfig.User = "ben";
      startAt = "*-*-* *:00:00";

      enable = true;
    };
  };

  programs.bash.completion.enable = true;

  # Feels weird to disable the below, but 24.11 demands it.
  # Doesn't seem to break anything. I love NixOS.
  /*sound.enable = true;

  hardware.pulseaudio = {
    enable = true;
    support32Bit = true;
  };*/

  hardware.bluetooth = {
    enable = true;
    powerOnBoot = true;
  };

  hardware.keyboard.zsa.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.11"; # Did you read the comment?

}
