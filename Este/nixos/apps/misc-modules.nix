{ pkgs, ...}:

let 
  run_wallpaper = pkgs.writeScriptBin "run_wallpaper" (builtins.readFile ./run_wallpaper.sh);
  view_wallpaper = pkgs.writeScriptBin "view_wallpaper" (builtins.readFile ./view_wallpaper.sh);
  xtoggle = pkgs.writeScriptBin "xtoggle" (builtins.readFile ./xtoggle.sh);
  cmushnotify = pkgs.writeScriptBin "cmushnotify" (builtins.readFile ./cmushnotify.sh);
in {
  environment.systemPackages = [ run_wallpaper view_wallpaper xtoggle cmushnotify ];
}
