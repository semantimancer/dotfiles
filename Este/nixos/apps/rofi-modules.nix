{ pkgs, ...}:

let 
  rofi-copy = pkgs.writeScriptBin "rofi-copy" (builtins.readFile ./rofi/copy.sh);
  rofi-goto = pkgs.writeScriptBin "rofi-goto" (builtins.readFile ./rofi/goto.sh);
  rofi-info = pkgs.writeScriptBin "rofi-info" (builtins.readFile ./rofi/info.sh);
  rofi-kill = pkgs.writeScriptBin "rofi-kill" (builtins.readFile ./rofi/kill.sh);
  rofi-qalc = pkgs.writeScriptBin "rofi-qalc" (builtins.readFile ./rofi/qalc.sh);
  rofi-search = pkgs.writeScriptBin "rofi-search" (builtins.readFile ./rofi/search.sh);
  rofi-shift = pkgs.writeScriptBin "rofi-shift" (builtins.readFile ./rofi/shift.sh);
  rofi-write = pkgs.writeScriptBin "rofi-write" (builtins.readFile ./rofi/write.sh);
in {
  environment.systemPackages = [ 
    rofi-copy 
    rofi-goto 
    rofi-info 
    rofi-kill 
    rofi-qalc 
    rofi-search 
    rofi-shift 
    rofi-write
  ];
}
