import Graphics.X11.Xlib
import Graphics.X11.Xlib.Extras
import System.Environment
import System.IO
import Data.Char

--All of this is adapted/stolen from the XMonad.Hooks.ServerMode documentation

main :: IO ()
main = parse True "XMONAD_COMMAND" =<< getArgs

parse :: Bool -> String -> [String] -> IO ()
parse input addr args = case args of
        ("-a":a:xs)     -> parse input a xs
        (a@('-':_):_)   -> hPutStrLn stderr ("Unknown option " ++ a)
        (x:xs)          -> sendCommand addr x >> parse False addr xs
        [] | input      -> hPutStrLn stderr "Missing arguments"
           | otherwise  -> return ()

sendCommand :: String -> String -> IO ()
sendCommand addr s = do
  d   <- openDisplay ""
  rw  <- rootWindow d $ defaultScreen d
  a <- internAtom d addr False
  m <- internAtom d s False
  allocaXEvent $ \e -> do
                  setEventType e clientMessage
                  setClientMessageEvent e rw a 32 m currentTime
                  sendEvent d rw False structureNotifyMask e
                  sync d False
