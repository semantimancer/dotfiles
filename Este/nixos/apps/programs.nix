{ config, pkgs, ... }:

with pkgs;

let
  kiosk = (
    { pkgs ? import <nixpkgs> {}, url, class, name ? url, ... }:
      pkgs.writeScriptBin name ''
        exec ${pkgs.firefox}/bin/firefox --new-window --kiosk ${pkgs.lib.escapeShellArg url} --class=${pkgs.lib.escapeShellArg class} -P ${pkgs.lib.escapeShellArg class}
      ''
  );
  homunculus = pkgs.haskellPackages.callPackage /home/ben/Computer/homunculus/homunculus.nix {};
  treasurer = pkgs.haskellPackages.callPackage /home/ben/Computer/treasurer/treasurer.nix {};
  xmonadctl = pkgs.haskellPackages.callPackage ./xmonadctl/xmonadctl.nix {};
  tex = (pkgs.texlive.combine { inherit (pkgs.texlive) scheme-medium enumitem multirow; });
  baseconfig = { allowUnfree = true; };
  #unstable = import <nixos-unstable> { config = baseconfig; };
in {
  nixpkgs.config = baseconfig // {
    packageOverrides = pkgs: {
      #qgis = unstable.qgis;
      #wine = unstable.wine;
      #obsidian = unstable.obsidian;
    };

    permittedInsecurePackages = [
      #"electron-19.1.9" # Discord
      #"electron-25.9.0" # YT Music
    ];
  };

  environment.systemPackages = with pkgs; [
    # Basics
    rxvt-unicode-unwrapped
    starship fzf zoxide bat ueberzug
    rofi xdotool wmctrl dunst libnotify
    gtop pciutils exfat linuxPackages.perf gparted
    rdiff-backup
    wget feh mupdf
    qalculate-gtk libqalculate
    unrar unzip zip p7zip
    wally-cli
    treasurer 

    appimage-run 
    (wine.override { wineBuild = "wineWow"; }) winetricks

    # Writing
    focuswriter libreoffice obsidian
    lyx tex pandoc

    qgis
    calibre

    # Coding
    git nix-prefetch-git direnv
    gcc ghc bundix

    # Graphics 
    xmonadctl xcompmgr redshift
    gimp scrot
    pscircle pywal
    cool-retro-term

    # Web
    floorp deluge
    (kiosk { name = "kiosk-youtube"; url = "youtube.com"; class = "movie"; })
    (kiosk { name = "kiosk-shudder"; url = "shudder.com"; class = "movie"; })
    (kiosk { name = "kiosk-twitch"; url = "twitch.tv"; class = "movie"; })
    # Discord doesn't respect non-updated clients. This circumvents the problem but is obnoxious.
    (discord.override { src = builtins.fetchTarball https://discord.com/api/download?platform=linux&format=tar.gz; })

    # Games
    crawl 
    homunculus

    # Media
    cmus yt-dlp
    picard ffmpeg 
    pamixer pavucontrol
    vlc handbrake mpv
  ];
}
