#!/usr/bin/env bash

if pgrep xcompmgr &>/dev/null; then
  echo "xcompmgr OFF"
  pkill xcompmgr &
else
  echo "xcompmgr ON"
  xcompmgr -F
fi

exit 0
