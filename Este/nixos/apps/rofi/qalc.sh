#!/usr/bin/env bash

if [ x"$@" = x"exit" ]
then
    exit 0
fi

if [ "$@" ]
then
    qalc $@ | tail -c 30 | awk '{print "\xE2\x80\xA6"$0}' >> /tmp/rofi-qalc
    tac /tmp/rofi-qalc | while read line ; do
      echo "$line"
    done
    echo "exit"
else
    rm /tmp/rofi-qalc
    echo -en "\x00prompt\x1fqalc\n"
    echo "exit"
fi
