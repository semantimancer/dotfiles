#!/usr/bin/env bash

if [ x"$@" = x"exit" ]
then
    exit 0
fi

if [ "$@" ]
then
    arr=($@)
    xmonadctl -a XMONAD_COPY ${arr[0]}
else
    echo -en "\x00prompt\x1fgoto\n"
    
    wmctrl -d | awk '{print $9}'
    
    echo "exit"
fi
