#!/usr/bin/env bash

# Adapted from tmpstpdwn's Quickinfo

# Options
user="🗣 User"
date="📅 Date"
time="🕒 Time"
volume="🔈 Volume"
uptime="⏰ Uptime"
dskuse="💾 Disk Usage"
kernel="🐧 Kernel Ver"

# Pass variables to rofi dmenu
echo "$user"
echo "$date"
echo "$time"
echo "$volume"
echo "$uptime"
echo "$dskuse"
echo "$kernel"

# Actions
#chosen="$(run_rofi)"
chosen="$@"

case ${chosen} in
    $user)
        dunstify "User" "$USER" -u low -r 4209 -i /tmp/sysinforofi.png
        ;;
    $date)
        dt="$(date | awk '{print $2" "$3" ("$1")"}')"
        dunstify "Date" "$dt" -u low -r 4209 -i /tmp/sysinforofi.png
        ;;
    $time)
        tm="$(date | awk '{print $4}')"
        dunstify "Time" "$tm" -u low -r 4209 -i /tmp/sysinforofi.png
        ;;
    $volume)
        vl="$(pamixer --get-volume-human)"
        dunstify "Volume" "$vl" -u low -r 4209 -i /tmp/sysinforofi.png
        ;;
    $uptime)
        ut="$(uptime | awk '{print substr($3, 1, length($3)-1)}')"
        dunstify "Uptime" "$ut" -u low -r 4209 -i /tmp/sysinforofi.png
        ;;
    $dskuse)
        ds="$(df -h --output=file,size,used,avail,pcent / /boot)"
        dunstify "Disk Usage" "$ds" -u low -r 4209 -i /tmp/sysinforofi.png
        ;;
    $kernel)
        dunstify "kernel" "$(uname -sr)" -u low -r 4209 -i /tmp/sysinforofi.png
        ;;
esac

# End
