#!/usr/bin/env bash

path_list=/home/ben/Documents/Writing/
paths=""

# Assumes two variables: run and file
function run_with_file {
  for i in ${path_list//,/ }
  do
    if [ -e "${i}$file" ]
    then
      run+=" ${i}$file"
    fi
  done
  coproc ($run >& /dev/null &)
}

if [ x"$@" = x"exit" ]
then
    exit 0
fi

if [ "$@" ]
then
    arr=($@)
    if [ ${arr[0]} == "focuswriter" ] || [ ${arr[0]} == "libreoffice" ] || [ ${arr[0]} == "lyx" ] || [ ${arr[0]} == "vim" ]
    then
        if [ ${arr[0]} == "vim" ]
        then
          run="urxvt -title ViM-Write -e vim"
        else
          run="${arr[0]}"
        fi
        if [ "${arr[1]}" ]
        then
          file=${arr[1]}
          run_with_file
        else
          coproc ($run >& /dev/null &)
        fi
        echo 
    else
      case $@ in
        *".txt"|*".md") 
          run="urxvt -title ViM-Write -e vim"
          file=$@
          run_with_file
        ;;
        *".lyx") 
          run="lyx"
          file=$@
          run_with_file
        ;;
        *)
          echo "focuswriter $@"
          echo "libreoffice $@"
          echo "lyx $@"
          echo "vim $@"
          echo "exit"
      esac
    fi
else
    echo -en "\x00prompt\x1fstart writing\n"
    echo "focuswriter"
    echo "libreoffice"
    echo "lyx"
    echo "vim"

    for i in ${path_list//,/ }
    do
      paths+=" $i"
      find $i -type f \( -name '*.txt' -o -name '*.rtf' -o -name '*.doc*' -o -name '*.lyx' \) -printf "%f\n"
    done

    echo "exit"
fi
