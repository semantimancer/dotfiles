syntax enable
set autoread
set nocompatible
set backspace=indent,eol,start

set nu
set rnu

set expandtab
set tabstop=2
set shiftwidth=2
set autoindent
set nojoinspaces
set showcmd
set incsearch
set ignorecase
set smartcase
set confirm
    
set wrap
set linebreak
set nolist
set textwidth=0
set wrapmargin=0

set pastetoggle=<F2>

set wildmode=longest,full
set wildmenu

set lazyredraw

command WC w !wc | awk '{print $2 " words" }'

nnoremap <expr> k (v:count == 0 ? 'gk' : 'k')
nnoremap <expr> j (v:count == 0 ? 'gj' : 'j')

nnoremap s "_d

nnoremap <C-d> <C-d>zz
nnoremap <C-u> <C-u>zz

" lightline
set laststatus=2
set noshowmode
let g:lightline = {
  \ 'colorscheme': 'jellybeans',
  \ 'active': {
  \   'right':  [ ['lineinfo'], ['percent'], ['filetype'], ['gitgutter'] ],
  \   'left':   [ ['mode', 'paste' ], ['readonly', 'filename', 'modified' ] ]
  \ },
  \ 'component_function': { 'gitgutter': 'GitStatus' },
  \ }

" gitgutter
function! GitStatus()
  let [a,m,r] = GitGutterGetHunkSummary()
  return printf('+%d ~%d -%d', a,m,r)
endfunction

highlight! link SignColumn LineNr

" NERDTree
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

nnoremap <Leader>f :NERDTreeToggle<CR>

" Limelight
let g:limelight_conceal_ctermfg = 'darkgray'
let g:limelight_paragraph_span = 1

" Goyo
function! s:goyo_enter()
  let b:quitting = 0
  let b:quitting_bang = 0
  autocmd QuitPre <buffer> let b:quitting = 1
  cabbrev <buffer> q! let b:quitting_bang = 1 <bar> q!
endfunction

function! s:goyo_leave()
  " Quit Vim if this is the only remaining buffer
  if b:quitting && len(filter(range(1, bufnr('$')), 'buflisted(v:val)')) == 1
    if b:quitting_bang
      qa!
    else
      qa
    endif
  endif
endfunction

" :q now closes both Goyo and Limelight if it's the only buffer
autocmd! User GoyoEnter call <SID>goyo_enter() | Limelight
autocmd! User GoyoLeave call <SID>goyo_leave() | Limelight!

nnoremap <Leader>g :Goyo<CR>
