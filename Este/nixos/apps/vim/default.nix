# Stolen from fern9001
#   https://gist.github.com/fern9001/0319d3e0c7c3fcab3008ecda0b3ddcf7
{ pkgs, ... }:

# make pkgs avaliable in lexical scope of the following expression
with pkgs;

# set the entire package as a local variable to include in environment.systemPackages
let myVim = vim_configurable.customize {
  name = "vim";
  
  vimrcConfig = {
    # import .vimrc
    customRC = builtins.readFile ./.vimrc;
    
    # make plugins avaliable to vam
    vam.knownPlugins = pkgs.vimPlugins;
    
    # declare plugins to use
    vam.pluginDictionaries = [
      { 
        names = [
          "gitgutter"
          "goyo"
          "lightline-vim"
          "limelight-vim"
          "vim-visual-multi"
          "nerdtree"
          #...
        ];
      }
    ];
  };
};
# include our customized vim package in systemPackages
in { 
  environment.systemPackages = with pkgs; [ myVim ]; 
  # set vim as default editor
  environment.variables = { EDITOR = "vim"; };
}
