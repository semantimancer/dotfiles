# NixOS setup

This configuration was adapted to be stateless via the [sanix](https://github.com/infinisil/sanix) template. As such `nix-channel` is no longer used, and instead Nixpkgs is maintained through [niv](https://github.com/nmattia/niv).

I don't use `nix-env` and only use `nix-shell` with commands I don't expect to use regularly, meaning this config should now be as close to plug-and-play as is possible.

## Usage

After making any changes as in the sections below, apply the changes with: 
```
sudo ./rebuild switch
```

All options to `./rebuild` are forwarded to `nixos-rebuild`.

After rebuilding, the changes are reflected in the system.

### Update Nixpkgs

```
niv update nixpkgs
```

### Upgrade to a newer release

```
niv update nixpkgs --branch nixos-xx.xx
```

### Regenerate Hardware Configuration

```
nixos-generate-configuration --dir .
```

### Other

- General changes are still made via `vim configuration.nix` or `vim apps/file-to-change`
- Nixpkgs config can be changed via `vim nixpkgs/config.nix`
- Nixpkgs overlays can be changed via `vim nixpkgs/overlays.nix`
