export EDITOR=vim
export VISUAL=vim
export PAGER="bat -p"
export TERMINAL=rxvt-unicode
export LANG=C
export BROWSER=floorp
export LC_ALL=C
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8
export STARSHIP_CONFIG=/home/ben/Computer/nixos/apps/starship.toml
export _ZO_EXCLUDE_DIRS=$HOME:/data*

export RTV_URLVIEWER=vivaldi

set -o vi

# Import colorscheme from 'wal' asynchronously
(cat ~/.cache/wal/sequences &)

# Finally, set up other programs w/bash
eval "$(direnv hook bash)"
eval "$(starship init bash)"
eval "$(zoxide init --cmd cd bash)"

# These save headaches later
alias ls='ls -hF --color=auto --group-directories-first'
alias rm='rm -I'
alias mv='mv -iv'
alias cp='cp -iv'
alias mkdir='mkdir -p -v'
alias ':q'='exit'
alias ':wq'='exit'

# Useful commands
alias cls='clear && ls'
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias ......='cd ../../../../..'
alias .......='cd ../../../../../..'
alias ........='cd ../../../../../../..'

# Make typing easier when SSHing via touchscreen device to change music
alias cmn='cmus-remote -n'
alias cmr='cmus-remote -r'
alias cmp='cmus-remote -u'
alias cmvu='cmus-remote -v +20%'
alias cmvd='cmus-remote -v -20%'
