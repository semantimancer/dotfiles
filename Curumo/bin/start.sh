#!/bin/bash

PS3="> "
opts=("Write" "Push" "Pull" "Shutdown" "Exit")

write(){
  echo "Opening ViM..."
  cd /home/pi/gdrive/Writing
  vim
  cd
}

screen(){
  clear
  echo "$(hostname) ($(hostname -I | awk ' { print $1 } '))"
  echo ""
}

screen
select opt in "${opts[@]}"; do
  case $opt in
    "Write")
      write
      ;;
    "Push")
      /home/pi/bin/gpush.sh
      echo "Press any key to continue..."
      read -t 3 -n 1
      ;;
    "Pull")
      /home/pi/bin/gpull.sh
      echo "Press any key to continue..."
      read -t 3 -n 1
      ;;
    "Shutdown")
      shutdown now
      sleep 10
      ;;
    "Exit")
      break
      ;;
    *)
      echo "Invalid option $opt"
      sleep 1
      ;;
  esac
  screen
  for (( i=0; i<${#opts[@]}; i++)); do
    echo "$(($i+1))) ${opts[$i]}"
  done
done
