syntax enable
colorscheme industry

set autoread
set nocompatible
set backspace=indent,eol,start

set nu
set rnu

set expandtab
set tabstop=2
set shiftwidth=2
set autoindent
set nojoinspaces
set showcmd
set incsearch
set ignorecase
set smartcase
set confirm
    
set wrap
set linebreak
set nolist
set textwidth=0
set wrapmargin=0

set pastetoggle=<F2>

set wildmode=longest,full
set wildmenu

set lazyredraw

nnoremap <expr> k (v:count == 0 ? 'gk' : 'k')
nnoremap <expr> j (v:count == 0 ? 'gj' : 'j')

nnoremap s "_d

" NERDTree
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

" Goyo
function! s:goyo_enter()
  let b:quitting = 0
  let b:quitting_bang = 0
  autocmd QuitPre <buffer> let b:quitting = 1
  cabbrev <buffer> q! let b:quitting_bang = 1 <bar> q!
endfunction

function! s:goyo_leave()
  " Quit Vim if this is the only remaining buffer
  if b:quitting && len(filter(range(1, bufnr('$')), 'buflisted(v:val)')) == 1
    if b:quitting_bang
      qa!
    else
      qa
    endif
  endif
endfunction

autocmd! User GoyoEnter call <SID>goyo_enter()
autocmd! User GoyoLeave call <SID>goyo_leave()

autocmd! Filetype text :Goyo

nnoremap <Leader>g :Goyo<CR>
