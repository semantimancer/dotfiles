{ config, pkgs, ... }:

with pkgs;

let
  kiosk = (
    { pkgs ? import <nixpkgs> {}, url, class, name ? url, ... }:
      pkgs.writeScriptBin name ''
        exec ${pkgs.firefox}/bin/firefox --new-window --kiosk ${pkgs.lib.escapeShellArg url} --class=${pkgs.lib.escapeShellArg class} -P ${pkgs.lib.escapeShellArg class}
      ''
  );
  homunculus = pkgs.haskellPackages.callPackage /home/ben/Computer/Homunculus/homunculus.nix {};
  treasurer = pkgs.haskellPackages.callPackage /home/ben/Computer/treasurer/treasurer.nix {};
  xmonadctl = pkgs.haskellPackages.callPackage /etc/nixos/apps/xmonadctl/xmonadctl.nix {};
  #xgifwallpaper = pkgs.callPackage /etc/nixos/apps/xgifwallpaper.nix {};
  tex = (pkgs.texlive.combine { inherit (pkgs.texlive) scheme-medium enumitem multirow; });
  baseconfig = { allowUnfree = true; };
  unstable = import <nixos-unstable> { config = baseconfig; };
in {
  nixpkgs.config = baseconfig // {
    packageOverrides = pkgs: {
      qgis = unstable.qgis;
      wine = unstable.wine;
      obsidian = unstable.obsidian;
    };

    permittedInsecurePackages = [
      "electron-19.1.9" # Discord
      "electron-25.9.0" # YT Music
    ];
  };

  environment.systemPackages = with pkgs; [
    # Basics
    lxterminal rxvt_unicode starship
    fzf zoxide
    rofi xdotool wmctrl
    gtop pciutils
    bat ueberzug
    rdiff-backup exfat linuxPackages.perf gparted
    wget feh mupdf
    appimage-run
    qalculate-gtk libqalculate
    unrar unzip zip
    polybar dunst libnotify
    deadd-notification-center
    vlc handbrake
    wally-cli
    treasurer gnumeric obsidian

    # Writing
    focuswriter libreoffice
    pandoc
    calibre
    lyx tex
    qgis

    # Coding
    git nix-prefetch-git direnv
    gcc ghc bundix
    etcher
    onefetch

    # Graphics 
    xmonadctl xcompmgr redshift #autorandr
    lxappearance shades-of-gray-theme hicolor-icon-theme gnome2.gnome_icon_theme
    gimp scrot
    pscircle pywal #xgifwallpaper
    cool-retro-term

    # Web
    firefox floorp
    mpv yt-dlp 
    (kiosk { name = "kiosk-youtube"; url = "youtube.com"; class = "movie"; })
    (kiosk { name = "kiosk-shudder"; url = "shudder.com"; class = "movie"; })
    (kiosk { name = "kiosk-twitch"; url = "twitch.tv"; class = "movie"; })
    deluge
    weather
    # Discord doesn't respect non-updated clients. This circumvents the problem but is obnoxious.
    (discord.override { src = builtins.fetchTarball https://discord.com/api/download?platform=linux&format=tar.gz; })

    # Games
    crawl 
    (wine.override { wineBuild = "wineWow"; }) winetricks
    homunculus
    (kiosk { name = "kiosk-forge"; url = "semantimancer.forge-vtt.com"; class = "forge"; })

    # Music
    cmus youtube-music
    picard ffmpeg 
    pamixer pavucontrol
    musescore supercollider jack2
    cava glava
  ];
}
