{ config, pkgs, ... }:

with pkgs;

let
  baseconfig = { allowUnfree = true; };
  unstable = import <nixos-unstable> { config = baseconfig; };
in {
  hardware.opengl.driSupport32Bit = true;
  hardware.opengl.extraPackages32 = with pkgs.pkgsi686Linux;
    [ libva ]
    ++ lib.optionals config.services.pipewire.enable [ pipewire ];

  # For the next time steam acts up
  # imports = [ <nixos-unstable/nixos/modules/programs/steam.nix> ];
  # disabledModules = [ "programs/steam.nix" ];

  programs.steam.enable = true;

  nixpkgs.config.packageOverrides = pkgs: {
    steam = pkgs.steam.override { extraPkgs = pkgs: with pkgs; [ pango harfbuzz libthai ]; };
  };

  environment.systemPackages = with pkgs; [
    steam-run 
    vulkan-headers vulkan-loader vulkan-tools vulkan-validation-layers
    protontricks
  ];
}
