{ lib, stdenv, fetchFromGitHub, rustPlatform, xorg }:

rustPlatform.buildRustPackage rec {
  pname = "xgifwallpaper";
  version = "0.3.1";

  src = fetchFromGitHub {
    owner = "calculon102";
    repo = pname;
    rev = "v${version}";
    sha256 = "sha256-QiaZJfilzzusAcB628khXF9ZwDWLDerMGIToIacEF+M=";
  };

  buildInputs = [ xorg.libX11 xorg.libXinerama xorg.libXext ];

  cargoSha256 = "sha256-mMkoAtmGHCBn6fAlLEH5JYdyju3lpnnNa/bvBnRBgbE=";
  verifyCargoDeps = true;

  meta = with lib; {
    description = "Use an animated GIF as wallpaper on X11 systems";
    homepage = "https://github.com/calculon102/xgifwallpaper";
    license = licenses.gpl3Only;
  };
}
