#!/usr/bin/env bash

declare -A urls
urls["duckduckgo"]="https://duckduckgo.com/?q="
urls["google"]="https://www.google.com/search?q="
urls["dictionary"]="https://www.dictionary.com/browse/"
urls["thesaurus"]="https://www.thesaurus.com/browse/"
urls["wikipedia"]="https://en.wikipedia.org/w/index.php?search="
urls["hoogle"]="https://www.haskell.org/hoogle/?hoogle="
urls["hackage"]="http://hackage.haskell.org/packages/search?terms="
urls["wolframalpha"]="http://www.wolframalpha.com/input/?i="
urls["imdb"]="http:/www.imdb.com/find?ref_=nv_sr_fn&q="
urls["youtube"]="https://www.youtube.com/results?search_query="
urls["rhymezone"]="https://rhymezone.com/r/rhyme.cgi?typeofrhyme=perfect&org1=syl&org2=l&org3=y&Word="

if [ x"$@" = x"exit" ]
then
    exit 0
fi

if [ "$@" ]
then
    arr=($@)
    found=0

    for i in "${!urls[@]}"
    do
        if [ ${arr[0]} == $i ]
        then
            found=1
        fi
    done

    if [ $found == 1 ] && [ ${arr[1]} ]
    then
        coproc (echo ${arr[@]:1} | xargs -I{} xdg-open ${urls[${arr[0]}]}{} >& /dev/null &)
        exit 0
    else
        for i in "${!urls[@]}"
        do
            echo "$i $@"
        done
    fi
else
    echo -en "\x00prompt\x1fsearch\n"
    echo "exit"
fi
