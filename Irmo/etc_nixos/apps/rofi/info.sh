#!/usr/bin/env bash

show_options(){
  echo "date"
  echo "time"
  echo "uptime"
  echo "currently playing"
  echo "exit"
}

if [ x"$@" = x"exit" ]
then
    exit 0
fi

if [[ "$@" = "date" ]]
then
    echo $(date +%a,\ %B\ %d)
    show_options
elif [[ "$@" = "time" ]]
then
    echo $(date +%H:%M:%S)
    show_options
elif [[ "$@" = "uptime" ]]
then
    echo $(uptime | awk '{ print $2 " " substr($3, 1, length($3)-1) }')
    show_options
elif [[ "$@" = "currently playing" ]]
then
    gpmdp_playback="/home/$(whoami)/.config/Google Play Music Desktop Player/json_store/playback.json"
    title=$(grep title "$gpmdp_playback" | awk '{ $1=""; print substr($0,3,length($0)-4) }')
    artist=$(grep artist "$gpmdp_playback" | awk '{ $1=""; print substr($0,3,length($0)-4) }')
    album=$(grep album\" "$gpmdp_playback" | awk '{ $1=""; print substr($0,3,length($0)-4) }')
    echo "> $title"
    echo ">   by $artist"
    echo ">   on $album"
    show_options
else
    echo -en "\x00prompt\x1finfo\n"
    show_options
fi
