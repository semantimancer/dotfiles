#!/usr/bin/env bash
defaults="dashboard web vim chat writing gimp work reference compile game music movie view upload pdf steam"

if [ x"$@" = x"exit" ]
then
    exit 0
fi

if [ "$@" ]
then
    arr=($@)
    xmonadctl -a XMONAD_GOTO ${arr[0]}
else
    echo -en "\x00prompt\x1fgoto\n"
    
    workspaces=$(wmctrl -d | awk '{print $9}')

    for d in $defaults
    do
      if [[ $workspaces != *$d* ]]; then
        workspaces="${workspaces} $d"
      fi
    done

    for w in $workspaces
    do
      echo "$w"
    done

    echo "exit"
fi
