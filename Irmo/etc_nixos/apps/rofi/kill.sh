#!/usr/bin/env bash

show_options(){
  ps -Ao comm k-pcpu h | head -20 > /tmp/rofi-kill
  cat /tmp/rofi-kill | while read line ; do
    echo "$line"
  done
}

if [ x"$@" = x"exit" ]
then
    exit 0
fi

if [ "$@" ]
then
    pgrep $@ | while read line ; do
      kill -9 $line
    done
    show_options
    echo "exit"
else
    echo -en "\x00prompt\x1fkill\n"
    show_options
    echo "exit"
fi
