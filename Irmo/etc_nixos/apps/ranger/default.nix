{ pkgs, ...}:

let wrapper = pkgs.writeScriptBin "ranger" ''
    #!${pkgs.stdenv.shell}
    exec ${pkgs.ranger}/bin/ranger -r /etc/nixos/apps/ranger
  '';
  myRanger = pkgs.symlinkJoin {
    name = "ranger";
    paths = [ wrapper pkgs.ranger ];
  };
in {
  environment.systemPackages = [ myRanger ];
}
