{ pkgs, mkDerivation, lib, base, ...}: 

mkDerivation {
  pname = "xmonadctl";
  version = "1.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [
    pkgs.haskellPackages.xmonad
    pkgs.haskellPackages.xmonad-contrib
    base
  ];
  license = lib.licenses.gpl3;
}
