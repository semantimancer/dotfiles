{ pkgs, ...}:

let 
  polybar-redshift = pkgs.writeScriptBin "polybar-redshift" (builtins.readFile ./polybar/redshift.sh);
  polybar-volume = pkgs.writeScriptBin "polybar-volume" (builtins.readFile ./polybar/volume.sh);
  polybar-weather = pkgs.writeScriptBin "polybar-weather" (builtins.readFile ./polybar/weather.sh);
  polybar-ping = pkgs.writeScriptBin "polybar-ping" (builtins.readFile ./polybar/ping.sh);
  polybar-cal = pkgs.writeScriptBin "polybar-cal" (builtins.readFile ./polybar/calendar.sh);
in {
  environment.systemPackages = [ polybar-redshift polybar-volume polybar-weather polybar-ping polybar-cal ];
}
