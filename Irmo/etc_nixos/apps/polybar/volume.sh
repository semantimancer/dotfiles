#!/bin/sh

volU="%{A4:pamixer -i 1:}"
volD="%{A5:pamixer -d 1:}"
toggle="%{A1:pamixer -t:}"

if [ "$(pamixer --get-mute)" == "true" ]
then
    echo "$toggle--%{A}"
else
    echo "$volU$volD$toggle$(pamixer --get-volume)%{A}%{A}%{A}"
fi
