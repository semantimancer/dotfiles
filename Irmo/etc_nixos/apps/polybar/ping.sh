#!/bin/sh

# I'm aware there are more efficient/elegant ways to do this. But writing it more
# explicitly makes it easier to decode in the future, and for a personal project
# that's worth more to me.

line=$(ping -c 1 -w 5 bkugler.com | awk 'NR==2 { gsub("time=",""); print $8 } ')

if [ $line ]
then
  echo "$line ms"
else
  echo "-- ms"
fi
