#!/usr/bin/env bash


CONFIG_DIR=~/.config/wallpaper/
CACHE_DIR=~/.cache/wallpaper/
LAST_FILE="${CACHE_DIR}last"
LOCK_FILE="${CACHE_DIR}lock"
DEFAULT_FILE="${CONFIG_DIR}default"
default_files=$(cat $DEFAULT_FILE)
file=""

export DISPLAY=:0
mkdir -p $CONFIG_DIR
mkdir -p $CACHE_DIR
touch $LOCK_FILE
touch $DEFAULT_FILE

rm "${CACHE_DIR}curr.jpg" &> /dev/null
rm "${CACHE_DIR}curr.png" &> /dev/null
rm "${CACHE_DIR}curr.gif" &> /dev/null

circle_file="${CACHE_DIR}circle.png"

run_pscircle(){
    pscircle --output=$circle_file --tree-center=-1000:0 --cpulist-center=-1450:-800 --memlist-center=-1000:-800 --tree-rotate=true --tree-rotation-angle=0.785 --background-color=303 --link-color-min=333 --link-color-max=9f9 --dot-color-min=a4a --dot-color-max=f4f --tree-font-color=bbb --toplists-font-color=bbb --toplists-pid-font-color=999 --toplists-bar-background=777 --toplists-bar-color=a4a
    wal -i "$circle_file" --saturate 0.6
}

if [[ "$@" = "lock" ]]
then
  echo "lock" > $LOCK_FILE
  echo "Locked! Run with 'unlock' to start again."
elif [[ "$@" = "unlock" ]]
then
  rm $LOCK_FILE
  echo "Unlocked! Run again to change."
else
  if [[ $(cat $LOCK_FILE) ]]
  then
    echo "Wallpaper locked!"
  else
    if [[ "$@" = "default" ]]
    then
      files=($default_files/*)
      echo "$default_files" > $LAST_FILE
    elif [[ "$@" ]]
    then
      realdir=$(realpath $@)
      files=($realdir/*)
      echo "$realdir" > $LAST_FILE
    elif [[ $(cat $LAST_FILE) ]]
    then
      last=$(cat $LAST_FILE)
      files=($last/*)
    else
      files=($default_files/*)
      echo "$default_files" > $LAST_FILE
    fi

    if [[ $file = "" ]]
    then
      file="${files[RANDOM % ${#files[@]}]}"
    fi

    case "$file" in
      # If it's a jpg or png, display it
      *.jpg*|*.jpeg*|*.png*)
        ln -sf "$file" "${CACHE_DIR}curr"
        wal -i "$file" --saturate 0.6
        ;;
      *.gif*)
        ln -sf "$file" "${CACHE_DIR}curr"
        wal -i "$file" --saturate 0.6
        xgifwallpaper -s MAX "$file" &
        ;;
      # If it's anything else, just run pscircle
      *)
        run_pscircle
        ;;
    esac
  fi
fi
