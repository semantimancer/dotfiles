{ pkgs, ...}:

let 
  run_wallpaper = pkgs.writeScriptBin "run_wallpaper" (builtins.readFile ./run_wallpaper.sh);
in {
  environment.systemPackages = [ run_wallpaper ];
}
