# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, lib, pkgs, ... }:

{
  # Boot
  imports =
    [ 
      ./apps/vim
      ./apps/ranger
      ./apps/steam.nix
      ./apps/polybar-modules.nix
      ./apps/rofi-modules.nix
      ./apps/misc-modules.nix
      ./apps/programs.nix
      # I'm choosing to ignore the hardware scan because I've seen it reset and lose my 
      # /boot directory twice now. Instead, I will manually migrate everything in 
      # hardware-configuration.nix into this file and mark it as such.
      # ./hardware-configuration.nix
      <nixpkgs/nixos/modules/installer/scan/not-detected.nix> # From hardware-configuration.nix
    ];

  # hardware-configuration.nix

  #boot.kernelPackages = pkgs.linuxPackages_latest;
  boot.initrd.availableKernelModules = [ "xhci_pci" "ahci" "usbhid" "sd_mod" "sr_mod" ];
  boot.kernelModules = [ "kvm-amd" ];
  boot.extraModulePackages = [ ];
  boot.loader.systemd-boot.configurationLimit = 10;

  fileSystems."/" =
    { device = "/dev/disk/by-uuid/d844d67e-8350-4824-8a77-308495384de6";
      fsType = "ext4";
    };

  fileSystems."/boot" =
    { device = "/dev/disk/by-uuid/FBA8-92AA";
      fsType = "vfat";
    };

  fileSystems."/mnt/external" = 
    { device = "/dev/disk/by-uuid/ae17522d-7454-4e75-aff7-c6403ca6d4d4";
      fsType = "ext4";
      options = [ "noauto" ];
    };

  swapDevices =
    [ { device = "/dev/disk/by-uuid/0746fc53-b5ec-4d2c-a7d6-a18357a6c53e"; }
    ];

  nix.settings.max-jobs = lib.mkDefault 12;
  boot.loader.systemd-boot.enable = true;
  networking.hostName = "Irmo";

  #services.nixosManual.showManual = true;
  services.lorri.enable = true;
  # Internationalization
  i18n.defaultLocale = "en_US.UTF-8";

  console.keyMap = "us";
  console.font = "Lat2-Terminus16";

  time.timeZone = "America/New_York";

  environment.homeBinInPath = true;

  fonts.packages = with pkgs; [
    corefonts 
    font-awesome_4
    (nerdfonts.override { fonts = [ "FiraCode" "FiraMono" "NerdFontsSymbolsOnly" ]; })
  ];

  # Graphics
  services.xserver.enable = true;
  services.xserver.autorun = true;

  services.xserver.windowManager.xmonad = {
    enable = true;
    enableContribAndExtras = true;
  };

  services.xserver.displayManager = {
    defaultSession = "none+xmonad";

    autoLogin.enable = true;
    autoLogin.user = "ben";

    lightdm.enable = true;
  };

  services.xserver.videoDrivers = [ "nvidia" ];

  services.autorandr = {
    enable = true;
    defaultTarget = "main";
  };

  # Sound
  sound.enable = true;

  hardware.pulseaudio = {
    enable = true;
    support32Bit = true;
    extraConfig = "unload-module module-switch-on-port-available\nunload-module module-switch-on-connect";
  };

  # Other
  hardware.keyboard.zsa.enable = true;

  services.openssh = {
    enable = true;
    # password's only good if you're at the keyboard
    settings.PasswordAuthentication = false;
    settings.KbdInteractiveAuthentication = false;
  };

  services.cron = {
    enable = true;
    systemCronJobs = [
      "*/15 * * * *      ben     run_wallpaper"
      "0    * * * *      ben     find ~/Music/ -type f -exec stat --printf '%Z\t%n\n' {} +|sort -n|tail -n 200|cut -f2 > ~/.config/cmus/playlists/Recently_Added"
    ];
  };


  system.autoUpgrade.enable = true;

  programs.bash.enableCompletion = true;

  # Users
  users.extraUsers.ben = {
    createHome = true;
    home = "/home/ben";
    description = "Ben Kugler";

    extraGroups = [ "wheel" "plugdev" ];

    isNormalUser = true;
    uid = 1000;

    openssh.authorizedKeys.keyFiles = [
      /home/ben/.ssh/authorized_keys
    ];
  };

  security.sudo = {
    enable = true; 
    configFile = "%wheel ALL=(ALL) ALL";
    extraRules = [{
      users = [ "ben" ];
      commands = [
        {
          command = "ALL";
          options = [ "NOPASSWD" ];
        }
      ];
    }];
   };

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "18.03"; # Did you read the comment?
}
